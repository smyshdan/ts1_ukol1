package test.java;
import main.java.Factorial;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FactorialTest {

    @Test
    public void testFactorial_5_120() {
        Assertions.assertEquals(120, Factorial.factorialRecursive(5));
    }

    @Test
    public void testFactorial_0_1() {
        Assertions.assertEquals(1, Factorial.factorialRecursive(0));
    }

    @Test
    public void testFactorial_1_1() {
        Assertions.assertEquals(1, Factorial.factorialRecursive(1));
    }
}
